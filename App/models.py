# <module 'sql_server.pyodbc' from 'C:\\Users\\mohamedabrar.a\\Desktop\\pycac\\lib\\site-packages\\sql_server\\pyodbc\\__init__.py'>
# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


# class Controllernodemapping(models.Model):
#     nodeid = models.AutoField(db_column='NodeId')  # Field name made lowercase.
#     nodename = models.CharField(db_column='NodeName', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     nodeip = models.CharField(db_column='NodeIp', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     controllername = models.CharField(db_column='ControllerName', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     controllerip = models.CharField(db_column='ControllerIp', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     isdeleted = models.CharField(db_column='IsDeleted', max_length=1, blank=True, null=True)  # Field name made lowercase.
#
#     class Meta:
#         managed = False
#         db_table = 'ControllerNodeMapping'


# class DiagnosisLevelTable(models.Model):
#     uid = models.AutoField()
#     projectid = models.CharField(db_column='projectID', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     dos_num = models.IntegerField(blank=True, null=True)
#     file_name = models.CharField(max_length=250, blank=True, null=True)
#     relevancy_score = models.FloatField(blank=True, null=True)
#     section = models.CharField(max_length=50, blank=True, null=True)
#     meat_verbiage = models.CharField(max_length=300, blank=True, null=True)
#     drug_found = models.CharField(max_length=300, blank=True, null=True)
#     highlight = models.IntegerField(blank=True, null=True)
#     search_term = models.TextField(blank=True, null=True)
#     icd_code = models.CharField(db_column='ICD_code', max_length=25, blank=True, null=True)  # Field name made lowercase.
#     code_confidence = models.FloatField(blank=True, null=True)
#     coordinates = models.CharField(max_length=50, blank=True, null=True)
#     page_num = models.IntegerField(blank=True, null=True)
#     line_num = models.IntegerField(blank=True, null=True)
#     code_type = models.CharField(max_length=25, blank=True, null=True)
#     hcc_code_flag = models.BooleanField(db_column='HCC_Code_Flag', blank=True, null=True)  # Field name made lowercase.
#     rx_code_flag = models.BooleanField(db_column='Rx_Code_Flag', blank=True, null=True)  # Field name made lowercase.
#     aca_code_flag = models.BooleanField(db_column='ACA_Code_Flag', blank=True, null=True)  # Field name made lowercase.
#     no_hcc_flag = models.BooleanField(db_column='no_HCC_Flag', blank=True, null=True)  # Field name made lowercase.
#     icd_9 = models.CharField(max_length=25, blank=True, null=True)
#     icd_16 = models.CharField(max_length=25, blank=True, null=True)
#     icd_17 = models.CharField(max_length=25, blank=True, null=True)
#     icd_18 = models.CharField(max_length=25, blank=True, null=True)
#     diag_issue = models.CharField(max_length=200, blank=True, null=True)
#     comment = models.CharField(max_length=200, blank=True, null=True)
#     combo_codes = models.IntegerField(blank=True, null=True)
#     netnewhcc = models.IntegerField(db_column='NetNewHCC', blank=True, null=True)  # Field name made lowercase.
#     icd_19 = models.CharField(max_length=100, blank=True, null=True)
#     category = models.IntegerField(blank=True, null=True)
#     is_comp = models.IntegerField(blank=True, null=True)
#     info = models.CharField(max_length=200, blank=True, null=True)
#     diag_line = models.CharField(max_length=500, blank=True, null=True)
#
#     class Meta:
#         managed = False
#         db_table = 'Diagnosis_Level_Table'


# class EncounterLevelTable(models.Model):
#     uid = models.AutoField()
#     projectid = models.CharField(db_column='projectID', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     file_name = models.CharField(max_length=500, blank=True, null=True)
#     providerid = models.CharField(db_column='providerID', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     provider_first_name = models.CharField(max_length=150, blank=True, null=True)
#     provider_middle_name = models.CharField(max_length=150, blank=True, null=True)
#     provider_last_name = models.CharField(max_length=150, blank=True, null=True)
#     provider_title = models.CharField(max_length=150, blank=True, null=True)
#     provider_confidence = models.IntegerField(blank=True, null=True)
#     provider_accepted = models.BooleanField(blank=True, null=True)
#     e_sign = models.CharField(max_length=50, blank=True, null=True)
#     dos_num = models.IntegerField(db_column='DOS_num', blank=True, null=True)  # Field name made lowercase.
#     dos_from_date = models.CharField(db_column='Dos_from_date', max_length=100, blank=True, null=True)  # Field name made lowercase.
#     dos_to_date = models.CharField(db_column='DOS_to_date', max_length=100, blank=True, null=True)  # Field name made lowercase.
#     dos_confidence = models.IntegerField(db_column='DOS_confidence', blank=True, null=True)  # Field name made lowercase.
#     dos_accepted = models.BooleanField(db_column='DOS_accepted', blank=True, null=True)  # Field name made lowercase.
#     start_page = models.CharField(max_length=110, blank=True, null=True)
#     end_page = models.CharField(max_length=110, blank=True, null=True)
#     encounter_confidence = models.IntegerField(db_column='Encounter_confidence', blank=True, null=True)  # Field name made lowercase.
#     claimid = models.CharField(db_column='claimID', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     claimid_confidence = models.IntegerField(db_column='claimID_confidence', blank=True, null=True)  # Field name made lowercase.
#     place_of_service = models.CharField(max_length=25, blank=True, null=True)
#     pos_confidence = models.IntegerField(db_column='POS_confidence', blank=True, null=True)  # Field name made lowercase.
#     dos_used = models.IntegerField(db_column='DOS_used', blank=True, null=True)  # Field name made lowercase.
#     dos_group = models.IntegerField(db_column='DOS_group', blank=True, null=True)  # Field name made lowercase.
#     encounter_comment_code = models.CharField(max_length=50, blank=True, null=True)
#     encounter_comments = models.CharField(max_length=500, blank=True, null=True)
#     auto_coded = models.IntegerField(blank=True, null=True)
#     auto_code_confidence = models.IntegerField(blank=True, null=True)
#     netnewhcc = models.IntegerField(db_column='NetNewHCC', blank=True, null=True)  # Field name made lowercase.
#     f2f = models.IntegerField(blank=True, null=True)
#     info = models.CharField(max_length=250, blank=True, null=True)
#
#     class Meta:
#         managed = False
#         db_table = 'Encounter_Level_Table'


# class FileLevelTable(models.Model):
#     id = models.AutoField()
#     file_name = models.CharField(max_length=250, blank=True, null=True)
#     total_dos = models.IntegerField(blank=True, null=True)
#     total_page = models.IntegerField(blank=True, null=True)
#     start_time = models.DateTimeField(blank=True, null=True)
#     end_time = models.DateTimeField(blank=True, null=True)
#     updated_on = models.DateTimeField(blank=True, null=True)
#     server_name = models.CharField(max_length=50, blank=True, null=True)
#     project_id = models.CharField(max_length=50, blank=True, null=True)
#     sys_id = models.CharField(max_length=50, blank=True, null=True)
#     error_file = models.CharField(max_length=25, blank=True, null=True)
#     file_path = models.CharField(max_length=250, blank=True, null=True)
#     error_flag = models.CharField(max_length=10, blank=True, null=True)
#     error_comment = models.CharField(max_length=255, blank=True, null=True)
#     pct_auto_coded = models.IntegerField(blank=True, null=True)
#     auto_coded = models.IntegerField(blank=True, null=True)
#     netnewhcc = models.IntegerField(db_column='NetNewHCC', blank=True, null=True)  # Field name made lowercase.
#
#     class Meta:
#         managed = False
#         db_table = 'File_Level_Table'


# class Multiocrfileinfo(models.Model):
#     mfid = models.BigAutoField(db_column='MFId', primary_key=True)  # Field name made lowercase.
#     chartid = models.BigIntegerField(db_column='ChartId')  # Field name made lowercase.
#     isignoreq = models.CharField(db_column='IsIgnoreQ', max_length=1, blank=True, null=True)  # Field name made lowercase.
#     isabbyyq = models.CharField(db_column='IsAbbyyQ', max_length=1, blank=True, null=True)  # Field name made lowercase.
#     istocrq = models.CharField(db_column='IsTocrQ', max_length=1, blank=True, null=True)  # Field name made lowercase.
#     abbyocrstatus = models.CharField(db_column='AbbyOcrStatus', max_length=1, blank=True, null=True)  # Field name made lowercase.
#     tocrstatus = models.CharField(db_column='TocrStatus', max_length=1, blank=True, null=True)  # Field name made lowercase.
#     ignoreqcount = models.IntegerField(db_column='IgnoreQCount', blank=True, null=True)  # Field name made lowercase.
#     abbyyqcount = models.IntegerField(db_column='AbbyyQCount', blank=True, null=True)  # Field name made lowercase.
#     tocrqcount = models.IntegerField(db_column='TocrQCount', blank=True, null=True)  # Field name made lowercase.
#     isreadytomerge = models.CharField(db_column='IsReadyToMerge', max_length=1, blank=True, null=True)  # Field name made lowercase.
#
#     class Meta:
#         managed = False
#         db_table = 'MultiOcrFileInfo'


# class Multiocrpagedetail(models.Model):
#     multipageuniqueid = models.BigAutoField(db_column='MultiPageUniqueId', primary_key=True)  # Field name made lowercase.
#     chartid = models.BigIntegerField(db_column='ChartId')  # Field name made lowercase.
#     pagename = models.CharField(db_column='PageName', max_length=500, blank=True, null=True)  # Field name made lowercase.
#     ocrprocessflag = models.CharField(db_column='OcrProcessFlag', max_length=1, blank=True, null=True)  # Field name made lowercase.
#     comments = models.CharField(db_column='Comments', max_length=3000, blank=True, null=True)  # Field name made lowercase.
#     isocrstatus = models.CharField(db_column='IsOCRStatus', max_length=1, blank=True, null=True)  # Field name made lowercase.
#     ocrpagestarttime = models.DateTimeField(db_column='OcrPageStartTime', blank=True, null=True)  # Field name made lowercase.
#     ocrpageendtime = models.DateTimeField(db_column='OcrPageEndTime', blank=True, null=True)  # Field name made lowercase.
#     ocrcompletedtime = models.DateTimeField(db_column='OcrCompletedTime', blank=True, null=True)  # Field name made lowercase.
#
#     class Meta:
#         managed = False
#         db_table = 'MultiOcrPageDetail'


class Ocrfileinfo(models.Model):
    chartid = models.BigAutoField(db_column='ChartId', primary_key=True)  # Field name made lowercase.
    chartname = models.CharField(db_column='ChartName', max_length=500, blank=True, null=True)  # Field name made lowercase.
    filetype = models.CharField(db_column='FileType', max_length=50, blank=True, null=True)  # Field name made lowercase.
    totalpagecount = models.IntegerField(db_column='TotalPageCount', blank=True, null=True)  # Field name made lowercase.
    preprocessstatus = models.CharField(db_column='PreProcessStatus', max_length=1, blank=True, null=True)  # Field name made lowercase.
    preprocstarttime = models.DateTimeField(db_column='PreProcStartTime', blank=True, null=True)  # Field name made lowercase.
    preprocendtime = models.DateTimeField(db_column='PreProcEndTime', blank=True, null=True)  # Field name made lowercase.
    preproccompdate = models.DateTimeField(db_column='PreProcCompDate', blank=True, null=True)  # Field name made lowercase.
    preprochostname = models.CharField(db_column='PreProcHostName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    hostname = models.CharField(db_column='HostName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    preocrstatus = models.CharField(db_column='PreOCRStatus', max_length=1, blank=True, null=True)  # Field name made lowercase.
    preocrstarttime = models.DateTimeField(db_column='PreOCRStartTime', blank=True, null=True)  # Field name made lowercase.
    preocrendtime = models.DateTimeField(db_column='PreOCREndTime', blank=True, null=True)  # Field name made lowercase.
    multiocrstatus = models.CharField(db_column='MultiOCRStatus', max_length=1, blank=True, null=True)  # Field name made lowercase.
    pagemergestatus = models.CharField(db_column='PageMergeStatus', max_length=1, blank=True, null=True)  # Field name made lowercase.
    mergestarttime = models.DateTimeField(db_column='MergeStartTime', blank=True, null=True)  # Field name made lowercase.
    mergeendtime = models.DateTimeField(db_column='MergeEndTime', blank=True, null=True)  # Field name made lowercase.
    filestatus = models.CharField(db_column='FileStatus', max_length=1, blank=True, null=True)  # Field name made lowercase.
    completeddate = models.DateTimeField(db_column='CompletedDate', blank=True, null=True)  # Field name made lowercase.
    projectid = models.CharField(db_column='ProjectId', max_length=50, blank=True, null=True)  # Field name made lowercase.
    projecttype = models.CharField(db_column='ProjectType', max_length=50, blank=True, null=True)  # Field name made lowercase.
    icdtype = models.CharField(db_column='IcdType', max_length=50, blank=True, null=True)  # Field name made lowercase.
    grouptype = models.CharField(db_column='GroupType', max_length=50, blank=True, null=True)  # Field name made lowercase.
    parallelqno = models.IntegerField(db_column='ParallelQNo', blank=True, null=True)  # Field name made lowercase.
    statuscode = models.CharField(db_column='StatusCode', max_length=50, blank=True, null=True)  # Field name made lowercase.
    comments = models.TextField(db_column='Comments', blank=True, null=True)  # Field name made lowercase.
    recstatus = models.CharField(db_column='RecStatus', max_length=1, blank=True, null=True)  # Field name made lowercase.
    createddate = models.DateTimeField(db_column='CreatedDate', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'OcrFileInfo'


# class OcrfileinfoBackup(models.Model):
#     chartid = models.BigAutoField(db_column='ChartId')  # Field name made lowercase.
#     chartname = models.CharField(db_column='ChartName', max_length=500, blank=True, null=True)  # Field name made lowercase.
#     filetype = models.CharField(db_column='FileType', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     totalpagecount = models.IntegerField(db_column='TotalPageCount', blank=True, null=True)  # Field name made lowercase.
#     preprocessstatus = models.CharField(db_column='PreProcessStatus', max_length=1, blank=True, null=True)  # Field name made lowercase.
#     preprocstarttime = models.DateTimeField(db_column='PreProcStartTime', blank=True, null=True)  # Field name made lowercase.
#     preprocendtime = models.DateTimeField(db_column='PreProcEndTime', blank=True, null=True)  # Field name made lowercase.
#     preproccompdate = models.DateTimeField(db_column='PreProcCompDate', blank=True, null=True)  # Field name made lowercase.
#     preprochostname = models.CharField(db_column='PreProcHostName', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     hostname = models.CharField(db_column='HostName', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     preocrstatus = models.CharField(db_column='PreOCRStatus', max_length=1, blank=True, null=True)  # Field name made lowercase.
#     preocrstarttime = models.DateTimeField(db_column='PreOCRStartTime', blank=True, null=True)  # Field name made lowercase.
#     preocrendtime = models.DateTimeField(db_column='PreOCREndTime', blank=True, null=True)  # Field name made lowercase.
#     multiocrstatus = models.CharField(db_column='MultiOCRStatus', max_length=1, blank=True, null=True)  # Field name made lowercase.
#     pagemergestatus = models.CharField(db_column='PageMergeStatus', max_length=1, blank=True, null=True)  # Field name made lowercase.
#     mergestarttime = models.DateTimeField(db_column='MergeStartTime', blank=True, null=True)  # Field name made lowercase.
#     mergeendtime = models.DateTimeField(db_column='MergeEndTime', blank=True, null=True)  # Field name made lowercase.
#     filestatus = models.CharField(db_column='FileStatus', max_length=1, blank=True, null=True)  # Field name made lowercase.
#     completeddate = models.DateTimeField(db_column='CompletedDate', blank=True, null=True)  # Field name made lowercase.
#     projectid = models.CharField(db_column='ProjectId', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     projecttype = models.CharField(db_column='ProjectType', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     icdtype = models.CharField(db_column='IcdType', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     grouptype = models.CharField(db_column='GroupType', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     parallelqno = models.IntegerField(db_column='ParallelQNo', blank=True, null=True)  # Field name made lowercase.
#     statuscode = models.CharField(db_column='StatusCode', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     comments = models.TextField(db_column='Comments', blank=True, null=True)  # Field name made lowercase.
#     recstatus = models.CharField(db_column='RecStatus', max_length=1, blank=True, null=True)  # Field name made lowercase.
#     createddate = models.DateTimeField(db_column='CreatedDate', blank=True, null=True)  # Field name made lowercase.
#
#     class Meta:
#         managed = False
#         db_table = 'OcrFileInfo_Backup'
#
#
# class Ocrfilestatus(models.Model):
#     id = models.AutoField(db_column='Id')  # Field name made lowercase.
#     ocrstatus = models.CharField(db_column='OcrStatus', max_length=5, blank=True, null=True)  # Field name made lowercase.
#     statusdescription = models.CharField(db_column='StatusDescription', max_length=500, blank=True, null=True)  # Field name made lowercase.
#
#     class Meta:
#         managed = False
#         db_table = 'OcrFileStatus'
#
#
# class Ocrpagedetail(models.Model):
#     pagedetailid = models.BigAutoField(db_column='PageDetailId', primary_key=True)  # Field name made lowercase.
#     chartid = models.BigIntegerField(db_column='ChartId')  # Field name made lowercase.
#     pagename = models.CharField(db_column='PageName', max_length=500, blank=True, null=True)  # Field name made lowercase.
#     ispreprocess = models.CharField(db_column='IsPreProcess', max_length=1, blank=True, null=True)  # Field name made lowercase.
#     dateofpreprocess = models.DateTimeField(db_column='DateOfPreProcess', blank=True, null=True)  # Field name made lowercase.
#     istesseract = models.CharField(db_column='IsTesseract', max_length=1)  # Field name made lowercase.
#     dateoftesseract = models.DateTimeField(db_column='DateOfTesseract', blank=True, null=True)  # Field name made lowercase.
#     confidenceresult = models.DecimalField(db_column='ConfidenceResult', max_digits=18, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
#     recocr = models.CharField(db_column='RecOCR', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     parallelqno = models.IntegerField(db_column='ParallelQNo', blank=True, null=True)  # Field name made lowercase.
#
#     class Meta:
#         managed = False
#         db_table = 'OcrPageDetail'
#
#
# class OcrpagedetailBackup(models.Model):
#     pagedetailid = models.BigAutoField(db_column='PageDetailId')  # Field name made lowercase.
#     chartid = models.BigIntegerField(db_column='ChartId')  # Field name made lowercase.
#     pagename = models.CharField(db_column='PageName', max_length=500, blank=True, null=True)  # Field name made lowercase.
#     ispreprocess = models.CharField(db_column='IsPreProcess', max_length=1, blank=True, null=True)  # Field name made lowercase.
#     dateofpreprocess = models.DateTimeField(db_column='DateOfPreProcess', blank=True, null=True)  # Field name made lowercase.
#     istesseract = models.CharField(db_column='IsTesseract', max_length=1)  # Field name made lowercase.
#     dateoftesseract = models.DateTimeField(db_column='DateOfTesseract', blank=True, null=True)  # Field name made lowercase.
#     confidenceresult = models.DecimalField(db_column='ConfidenceResult', max_digits=18, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
#     recocr = models.CharField(db_column='RecOCR', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     parallelqno = models.IntegerField(db_column='ParallelQNo', blank=True, null=True)  # Field name made lowercase.
#
#     class Meta:
#         managed = False
#         db_table = 'OcrPageDetail_Backup'
#
#
# class PageLevelTable(models.Model):
#     uid = models.AutoField()
#     projectid = models.CharField(db_column='projectID', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     file_name = models.CharField(max_length=250, blank=True, null=True)
#     ocr_confidence = models.FloatField(db_column='OCR_confidence', blank=True, null=True)  # Field name made lowercase.
#     page_type = models.CharField(max_length=50, blank=True, null=True)
#     handwritten = models.CharField(max_length=50, blank=True, null=True)
#     number_of_lines = models.IntegerField(blank=True, null=True)
#     page_num = models.IntegerField(blank=True, null=True)
#     page_conf_warning = models.BooleanField(blank=True, null=True)
#     page_conf_warning_text = models.CharField(max_length=100, blank=True, null=True)
#     display_warning = models.IntegerField(blank=True, null=True)
#     width = models.CharField(max_length=50, blank=True, null=True)
#     height = models.CharField(max_length=50, blank=True, null=True)
#     ecw = models.IntegerField(blank=True, null=True)
#     pt_year_exists = models.IntegerField(blank=True, null=True)
#     pt_last_name_exists = models.IntegerField(blank=True, null=True)
#     encounter_num = models.IntegerField(db_column='Encounter_num', blank=True, null=True)  # Field name made lowercase.
#
#     class Meta:
#         managed = False
#         db_table = 'Page_Level_Table'
#
#
# class Testparam(models.Model):
#     testxmlcol = models.TextField(blank=True, null=True)  # This field type is a guess.
#
#     class Meta:
#         managed = False
#         db_table = 'Testparam'
#
#
# class Sysdiagrams(models.Model):
#     name = models.CharField(max_length=128)
#     principal_id = models.IntegerField()
#     diagram_id = models.AutoField(primary_key=True)
#     version = models.IntegerField(blank=True, null=True)
#     definition = models.BinaryField(blank=True, null=True)
#
#     class Meta:
#         managed = False
#         db_table = 'sysdiagrams'
#         unique_together = (('principal_id', 'name'),)
