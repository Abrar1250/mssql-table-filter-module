from django.shortcuts import render
# from django.http import HttpResponse
# from django.http import Http404
# from .models import Test
from website.settings import conn

# def index():
#     my_model = Test.objects.all()
#     print(my_model)
#     # All_Data = Test.objects.all()
#     # print(All_Data)
#     bar_chart = pygal.Bar()
#     bar_chart.add('Fibonacci', [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55])
#     context = {
#         "All_Data": bar_chart,
#     }
#     return render(request,'App/index.html', context)

def detail(request):
    cursor = conn.cursor()
    sql = "EXEC [dbo].[OcrFileInfo];"
    params = ("WGSWST011")
    row = cursor.execute("select * from [dbo].[OcrFileInfo]")
    row = row.fetchall()
    # chartid = ""
    ChartName = ""
    PrjId = ""
    PrjType = ""
    GrpType = ""
    IcdType = ""
    Prj = ""
    out = []
    for each_row in row:
        chartid = each_row[0]

        # print(type(chartid), "chartid")
        ChartName = each_row[1]
        # print(type(ChartName), "ChartName")
        FileType = each_row[2]
        Page_count = str(each_row[3])
        HostName = each_row[9]
        PrjId = each_row[19]
        # print(type(PrjId), "PrjId")
        PrjType = each_row[20]
        # print(type(PrjType), "PrjType")
        GrpType = each_row[22]
        # print(type(GrpType), "GrpType")
        IcdType = each_row[21]
        # print(type(IcdType), "IcdType")
        Prj = PrjId + '_' + PrjType + '_' + IcdType + '_' + GrpType
        # print(type(Prj), "Prj")
        print(ChartName, chartid, PrjId, PrjType, GrpType, IcdType, Prj)
        # out.extend([ChartName, chartid, PrjId, PrjType, GrpType, IcdType, Prj])
        out.append({'ChartName':ChartName,"HostName":HostName,"FileType":FileType,"Page_count":Page_count, 'chartid':chartid, 'PrjId':PrjId, 'PrjType':PrjType, 'GrpType':GrpType, 'IcdType':IcdType, 'Prj':Prj})
    conn.close()
    print (out)
    # return render(request, 'App/detail.html', {
    #         "ChartName": ChartName,
    #         "PrjId": PrjId, "PrjType": PrjType,
    #         "GrpType": GrpType, "IcdType": IcdType,
    #         "Prj": Prj})
    return render(request, 'App/detail.html', { "out" : out } )

# x = detail()

# def chart_detail():
#     my_model = Test.objects.values('FireFox')[0]
#     print(my_model)
    # line_chart = pygal.Bar()
    # line_chart.title = 'Browser usage evolution (in %)'
    # line_chart.x_labels = map(str, range(2002, 2013))
    # line_chart.add('Firefox', [None, None, 0, 16.6,   25,   31, 36.4, 45.5, 46.3, 42.8, 37.1])
    # line_chart.add('Chrome',  [None, None, None, None, None, None,    0,  3.9, 10.8, 23.8, 35.3])
    # line_chart.add('IE',      [85.8, 84.6, 84.7, 74.5,   66, 58.6, 54.7, 44.8, 36.2, 26.6, 20.1])
    # line_chart.add('Others',  [14.2, 15.4, 15.3,  8.9,    9, 10.4,  8.9,  5.8,  6.7,  6.8,  7.5])
    # return render(request, 'App/chart.html', {'output': line_chart.render()})

# x = chart_detail()